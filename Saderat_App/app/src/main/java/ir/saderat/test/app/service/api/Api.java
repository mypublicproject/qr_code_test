package ir.saderat.test.app.service.api;

import io.reactivex.Single;
import ir.saderat.test.app.service.model.ResponseModel;
import ir.saderat.test.app.service.url.Const;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api
{
    @GET(Const.GetList)
    Single<ResponseModel> getResponse(
            @Query("page") Integer page
    );
}
