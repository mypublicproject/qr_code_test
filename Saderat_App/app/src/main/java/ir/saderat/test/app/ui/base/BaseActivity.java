package ir.saderat.test.app.ui.base;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import lombok.Getter;
import lombok.Setter;

public class BaseActivity extends AppCompatActivity
{

    @Getter @Setter
    private Fragment fragment, mainFragment;

    @Getter @Setter
    private FragmentManager myFragmentManager;

    @Getter @Setter
    private FragmentTransaction transaction;

    @Getter @Setter
    private  @IdRes
    int containerViewId;


    protected void addFragment(Fragment fragment, String tag)
    {
        transaction = myFragmentManager.beginTransaction();
        transaction.add(getContainerViewId(), fragment, tag)
                .commitAllowingStateLoss();
    }

    protected void replaceFragment(Fragment fragment, String tag)
    {
        transaction = myFragmentManager.beginTransaction();
        transaction.replace(getContainerViewId(), fragment, tag)
                .commitAllowingStateLoss();
    }

}
