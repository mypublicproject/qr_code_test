package ir.saderat.test.app.ui.fragment

import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import ir.saderat.test.app.R
import ir.saderat.test.app.service.model.DataModel
import ir.saderat.test.app.ui.activity.MainActionView
import org.greenrobot.eventbus.EventBus
import java.util.regex.Pattern


class QRCodeFragment(val contentLayoutId: Int) : Fragment(contentLayoutId)
{
    private var rootView: View? = null
    private var mListener: MainActionView? = null
    private lateinit var codeScanner: CodeScanner

    companion object
    {
        @JvmStatic
        fun newInstance(listener: MainActionView): QRCodeFragment
        {
            val fragment = QRCodeFragment(R.layout.fragment_qr)
            fragment.mListener = listener
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        rootView = inflater.inflate(contentLayoutId, container, false)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        val scannerView = view.findViewById<CodeScannerView>(R.id.scanner_view)
        val activity = requireActivity()
        codeScanner = CodeScanner(activity, scannerView)
        codeScanner.decodeCallback = DecodeCallback {

            sendData(it.text)

            activity.runOnUiThread {
                Toast.makeText(activity, "${it.text} was added!", Toast.LENGTH_LONG).show()
            }
        }

        codeScanner.startPreview()
    }

    private fun sendData(text: String?)
    {
        if (isValidation(text))
        {
            var model = DataModel()
            model.id = 0
            model.image = ""
            model.email = text

            val splitterMail = "@"
            val fulName = text!!.split(splitterMail)[0]
            var splitterName = ""

            if (text.contains("."))
            {
                splitterName = "."
            }
            else if (text.contains("-"))
            {
                splitterName = "-"
            }
            else if (text.contains("_"))
            {
                splitterName = "_"
            }

            if (splitterName == "")
            {
                model.firstName = fulName
                model.lastName = ""
            }
            else if (fulName.split(splitterName).size >= 2)
            {
                model.firstName = fulName.split(splitterName)[0]
                var lname = ""
                fulName.split(splitterName)
                    .forEach {

                    if (it != model.firstName)
                    {
                        lname += "$it "
                    }
                }
                model.lastName = lname
            }
            EventBus.getDefault().post(model)
            activity!!.runOnUiThread { mListener!!.backToMainFragment() }
        }
    }

    private fun isValidation(text:String?): Boolean
    {
        var err = false
//        if (text!!.matches("^[a-zA-Z._-][@]^[a-zA-Z][.]^[a-zA-Z]".toRegex()))
        val pattern: Pattern = Patterns.EMAIL_ADDRESS
        if (pattern.matcher(text).matches())
        {
            err = true
        }

        Log.e("-isValidation-", err.toString())

        return err
    }

    override fun onResume()
    {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause()
    {
        codeScanner.releaseResources()
        super.onPause()
    }

}