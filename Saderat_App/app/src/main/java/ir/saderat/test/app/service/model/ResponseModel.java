package ir.saderat.test.app.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseModel
{
    @Expose
    @SerializedName("ad")
    private Ad ad;

    @Expose
    @SerializedName("data")
    private List<DataModel> dataModelList;

    @Expose
    @SerializedName("total_pages")
    private int total_pages;

    @Expose
    @SerializedName("total")
    private int total;

    @Expose
    @SerializedName("per_page")
    private int per_page;

    @Expose
    @SerializedName("page")
    private int page;

    public Ad getAd()
    {
        return ad;
    }

    public void setAd(Ad ad)
    {
        this.ad = ad;
    }

    public List<DataModel> getDataModelList()
    {
        return dataModelList;
    }

    public void setDataModelList(List<DataModel> dataModelList)
    {
        this.dataModelList = dataModelList;
    }

    public int getTotal_pages()
    {
        return total_pages;
    }

    public void setTotal_pages(int total_pages)
    {
        this.total_pages = total_pages;
    }

    public int getTotal()
    {
        return total;
    }

    public void setTotal(int total)
    {
        this.total = total;
    }

    public int getPer_page()
    {
        return per_page;
    }

    public void setPer_page(int per_page)
    {
        this.per_page = per_page;
    }

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

}
