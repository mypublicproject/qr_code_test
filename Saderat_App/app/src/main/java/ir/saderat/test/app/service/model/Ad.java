package ir.saderat.test.app.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ad
{
    @Expose
    @SerializedName("text")
    private String text;

    @Expose
    @SerializedName("url")
    private String url;

    @Expose
    @SerializedName("company")
    private String company;

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }
}
