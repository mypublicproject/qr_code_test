package ir.saderat.test.app.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataModel implements Parcelable
{
    @Expose
    @SerializedName("avatar")
    private String image;

    @Expose
    @SerializedName("last_name")
    private String lastName;

    @Expose
    @SerializedName("first_name")
    private String firstName;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("id")
    private int id;

    public DataModel()
    {
    }

    protected DataModel(Parcel in)
    {
        image = in.readString();
        lastName = in.readString();
        firstName = in.readString();
        email = in.readString();
        id = in.readInt();
    }

    public static final Creator<DataModel> CREATOR = new Creator<DataModel>()
    {
        @Override
        public DataModel createFromParcel(Parcel in)
        {
            return new DataModel(in);
        }

        @Override
        public DataModel[] newArray(int size)
        {
            return new DataModel[size];
        }
    };

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(image);
        parcel.writeString(lastName);
        parcel.writeString(firstName);
        parcel.writeString(email);
        parcel.writeInt(id);
    }
}
