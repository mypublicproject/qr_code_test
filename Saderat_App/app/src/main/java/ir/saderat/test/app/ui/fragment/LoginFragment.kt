package ir.saderat.test.app.ui.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import ir.saderat.test.app.R
import org.greenrobot.eventbus.EventBus

class LoginFragment(val contentLayoutId: Int): Fragment(contentLayoutId)
{
    private var rootView: View? = null
    private var btnLogin: Button? = null
    private var layout: RelativeLayout? = null
    private var rlContent: RelativeLayout? = null
    private var user_name: EditText? = null
    private var pass: EditText? = null

    private var isLogin:Boolean = false

    companion object
    {
        @JvmStatic
        fun newInstance(isLogin: Boolean):LoginFragment
        {
            val fragment = LoginFragment(R.layout.fragment_login)
            val arg = Bundle()
            arg.putBoolean("isLogin", isLogin)
            fragment.arguments = arg
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        isLogin = arguments!!.getBoolean("isLogin")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        rootView = inflater.inflate(contentLayoutId, container, false)
        btnLogin = rootView!!.findViewById(R.id.btn_login)
        layout = rootView!!.findViewById(R.id.container)
        rlContent = rootView!!.findViewById(R.id.rlContent)
        user_name = rootView!!.findViewById(R.id.user_name)
        pass = rootView!!.findViewById(R.id.user_password)

        if (isLogin)
        {
            rlContent!!.visibility = View.GONE
            layout!!.setBackgroundColor(Color.parseColor("#00ff00"))
        }

        btnLogin!!.setOnClickListener {
            if (!user_name!!.text.toString().equals("", true) && !pass!!.text.toString().equals("", true))
            {
                rlContent!!.visibility = View.GONE
                layout!!.setBackgroundColor(Color.parseColor("#00ff00"))
                EventBus.getDefault().post(true)
            }
        }

        return rootView
    }

}