package ir.saderat.test.app.ui.activity

import com.google.android.material.bottomnavigation.BottomNavigationView

interface MainActionView
{
    fun showLoading()

    fun hideLoading()

    fun backToMainFragment()

    fun setCheckedBNV(bottomNavigationView: BottomNavigationView, index: Int)
}