package ir.saderat.test.app.ui.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import ir.saderat.test.app.R
import ir.saderat.test.app.service.model.DataModel

class MainListAdapter(val context: Context, val itemList: List<DataModel>, val listener: MyListener?): RecyclerView.Adapter<MainListAdapter.ViewHolder>()
{
//    val listener: MyListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_main, null))
    }

    override fun getItemCount(): Int
    {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        val item = itemList.get(position)

        holder.tvTitle.text = "${item.firstName} ${item.lastName}"
        holder.tvEmail.text = item.email

        setImageBackground(holder.image, item.image)
    }

    private fun setImageBackground(imageView: ImageView, link: String)
    {
        try
        {
            Picasso.with(context).load(Uri.parse(link))
                .into(imageView, object : Callback
                {
                    override fun onSuccess()
                    {
                    }

                    override fun onError()
                    {
                        Picasso.with(context).load(R.drawable.img_failure).into(imageView)
                    }
                })
        }
        catch (e: NullPointerException)
        {
            Picasso.with(context).load(R.drawable.img_failure).into(imageView)
        }
    }

    inner class ViewHolder(var view: View): RecyclerView.ViewHolder(view)
    {
        var rootView: RelativeLayout = view.findViewById(R.id.rootView)
        var tvTitle: TextView = view.findViewById(R.id.tvTitle)
        var tvEmail: TextView = view.findViewById(R.id.tvEmail)
        var image: ImageView = view.findViewById(R.id.image)

        init
        {
            rootView!!.setOnClickListener {

                listener!!.onClick(adapterPosition, itemList[adapterPosition].id, "${itemList[adapterPosition].firstName} ${itemList[adapterPosition].lastName}")
            }
        }

    }

    interface MyListener
    {
        fun onClick(position: Int?, id: Int?, fullName:String?)
    }

}