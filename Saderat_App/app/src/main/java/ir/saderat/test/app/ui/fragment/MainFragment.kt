package ir.saderat.test.app.ui.fragment

import android.content.Context
import android.nfc.tech.MifareUltralight.PAGE_SIZE
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ir.saderat.test.app.R
import ir.saderat.test.app.service.api.Api
import ir.saderat.test.app.service.model.DataModel
import ir.saderat.test.app.service.model.ResponseModel
import ir.saderat.test.app.service.retrofit.MyRetrofit
import ir.saderat.test.app.ui.adapter.MainListAdapter
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*


class MainFragment(val contentLayoutId: Int): Fragment(contentLayoutId), MainListAdapter.MyListener
{
    private val compositeDisposable = CompositeDisposable()
    private var list: MutableList<DataModel> = mutableListOf()

    private var rootView: View? = null
    private var adapter: MainListAdapter? = null
    private var recyclerView: RecyclerView? = null
    private var progress: ProgressBar? = null
    private val layoutManager = LinearLayoutManager(context)

    private lateinit var mContext: Context
    private var currentPage:Int = 0
    private var totalPage:Int = 1
    private var pageSize:Int = 1
    private var totalItemSize:Int = 1
    private var isLoading:Boolean = false
    private var isLastPage:Boolean = false

    private val recyclerViewOnScrollListener: RecyclerView.OnScrollListener =
        object : RecyclerView.OnScrollListener()
        {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int)
            {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int)
            {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount: Int = layoutManager.getChildCount()
                val totalItemCount: Int = layoutManager.getItemCount()
                val firstVisibleItemPosition: Int = layoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastPage)
                {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE
                    )
                    {
                        loadMoreItems()
                    }
                }
            }
        }

    companion object
    {
        @JvmStatic
        fun newInstance(list: List<DataModel>?):MainFragment
        {
            val fragment = MainFragment(R.layout.fragment_main)
            val arg = Bundle()
            arg.putParcelableArrayList("DataModel", list as ArrayList<out Parcelable>)
            fragment.arguments = arg
            return fragment
        }
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        if (arguments != null)
        {
            list.addAll(arguments!!.getParcelableArrayList("DataModel"))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        rootView = inflater.inflate(contentLayoutId, container, false)

        recyclerView = rootView!!.findViewById(R.id.recyclerView)
        progress = rootView!!.findViewById(R.id.progress)

        recyclerView!!.layoutManager = layoutManager
//        recyclerView!!.itemAnimator = SlideInUpAnimator()
        recyclerView!!.addOnScrollListener(recyclerViewOnScrollListener)

        loadMoreItems()

        return rootView
    }

    private fun loadMoreItems()
    {
        if (currentPage != totalPage)
        {
            currentPage++
            isLoading = true
            progress!!.visibility = View.VISIBLE

            val api: Api = MyRetrofit.getRetrofit().create(Api::class.java)

            compositeDisposable.add(
                api.getResponse(currentPage)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ response -> onResponse(response) }, { t -> onFailure(t) })
            )
        }
    }

    private fun onResponse(response: ResponseModel)
    {
        progress!!.visibility = View.GONE
        isLoading = false

        pageSize = response.per_page
        totalPage = response.total_pages
        totalItemSize = response.total

        if (currentPage == totalPage) isLastPage = false

        list.addAll(response.dataModelList)
        adapter = MainListAdapter(mContext, list, this)
        recyclerView!!.adapter = adapter

    }

    private fun onFailure(t: Throwable)
    {
        Toast.makeText(context, t.message, Toast.LENGTH_SHORT).show()
        progress!!.visibility = View.GONE
    }

    override fun onClick(position: Int?, id: Int?, fullName: String?)
    {
        Toast.makeText(context, fullName, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy()
    {
        compositeDisposable.dispose()
        super.onDestroy()
    }

}