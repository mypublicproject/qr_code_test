package ir.saderat.test.app.ui.activity;

import android.Manifest;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ir.saderat.test.app.R;
import ir.saderat.test.app.service.model.DataModel;
import ir.saderat.test.app.ui.base.BaseActivity;
import ir.saderat.test.app.ui.fragment.LoginFragment;
import ir.saderat.test.app.ui.fragment.MainFragment;
import ir.saderat.test.app.ui.fragment.QRCodeFragment;

public class MainActivity extends BaseActivity implements MainActionView
{
    private final int Navigation_Default_Position = 1;
    private List<DataModel> list = new ArrayList<>();
    private Boolean isLoggedon = false;

    private BottomNavigationView bottomNavigationView;
    private Toolbar mToolbar;
    private TextView tvTitle;

    private Boolean isMainFragment = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        EventBus.getDefault().register(this);
    }

    private void initView()
    {
        mToolbar = findViewById(R.id.toolbar);
        tvTitle = mToolbar.findViewById(R.id.tvTitle);

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.getMenu().getItem(Navigation_Default_Position).setChecked(true);
        tvTitle.setText(bottomNavigationView.getMenu().getItem(Navigation_Default_Position).getTitle());

        setMyFragmentManager(getSupportFragmentManager());
        setContainerViewId(R.id.main_container);

        backToMainFragment();

        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem ->
        {
            Log.e("--item--", menuItem.getTitle().toString());
            int itemId = menuItem.getItemId();
            switch (itemId)
            {
                case R.id.tab_qr:
                {
                    if (!bottomNavigationView.getMenu().getItem(2).isChecked())
                    {
                        new TedPermission(getApplicationContext())
                                .setPermissionListener(new PermissionListener()
                                {
                                    @Override
                                    public void onPermissionGranted()
                                    {
                                        setCheckedBNV(bottomNavigationView, 2);

                                        isMainFragment = false;

                                        setFragment(QRCodeFragment.Companion.newInstance(MainActivity.this));
                                        replaceFragment(getFragment(), "qrCodeFragment");
                                    }

                                    @Override
                                    public void onPermissionDenied(ArrayList<String> deniedPermissions)
                                    {
                                        backToMainFragment();
                                    }
                                })
                                .setPermissions(Manifest.permission.CAMERA)
                                .check();
                    }
                    break;
                }
                case R.id.tab_home:
                {
                    if (!bottomNavigationView.getMenu().getItem(1).isChecked())
                    {
                        backToMainFragment();
                    }
                    break;
                }
                case R.id.tab_login:
                {
                    if (!bottomNavigationView.getMenu().getItem(0).isChecked())
                    {
                        setCheckedBNV(bottomNavigationView, 0);

                        isMainFragment = false;

                        setFragment(LoginFragment.Companion.newInstance(isLoggedon));
                        replaceFragment(getFragment(), "loginFragment");
                    }
                    break;
                }
            }


            return true;
        });
    }

    @Override
    public void backToMainFragment()
    {
        Log.e("--backToMainFragment--", "Called...");

        setCheckedBNV(bottomNavigationView, Navigation_Default_Position);
        isMainFragment = true;

        try
        {
            setMainFragment(MainFragment.Companion.newInstance(list));
            replaceFragment(getMainFragment(), "mainFragment");
        }
        catch (Exception e)
        {
            runOnUiThread(() ->
            {
                setMainFragment(MainFragment.Companion.newInstance(list));
                replaceFragment(getMainFragment(), "mainFragment");
            });
            e.printStackTrace();
        }
    }

    @Override
    public void setCheckedBNV(BottomNavigationView bottomNavigationView, int index)
    {
        for (int i = 0; i < 3; i++)
        {
            if (i == index)
            {
                bottomNavigationView.getMenu().getItem(index).setChecked(true);
                tvTitle.setText(bottomNavigationView.getMenu().getItem(index).getTitle());
            }
            else
            {
                bottomNavigationView.getMenu().getItem(index).setChecked(false);
                tvTitle.setText(bottomNavigationView.getMenu().getItem(index).getTitle());
            }
        }
    }

    @Override
    public void showLoading()
    {

    }

    @Override
    public void hideLoading()
    {

    }

    @Subscribe
    public void addItem(DataModel item)
    {
        list.add(item);
        Collections.reverse(list);
    }

    @Subscribe
    public void isLoggedOn(Boolean item)
    {
        isLoggedon = item;
    }

    @Override
    protected void onDestroy()
    {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
